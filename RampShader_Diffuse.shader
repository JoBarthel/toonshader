﻿Shader "Unlit/TexturedToonShader_Diffuse"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        
        [Header(Shadows)]
        [NoScaleOffset]
        _HalfShadowTexture("Half Shadow", 2D) = "white" {}
        [NoScaleOffset]
        _ShadowTexture ("Shadow", 2D) = "white" {}

        
        _ShadowToHalfThreshold("Shadow to half threshold", Range(0, 1)) = .3
        _HalfToLightThreshold("Half to light threshold", Range(0, 1)) = .8
        
        [Header(Rim Lighting)]
        _RimColor("Rim Color", Color) = (1,1,1,1)
        _RimAmount("Rim Amount", Range(0, 1)) = 0.716
        _RimThreshold("Rim threshold", Range(0, 1)) = .2
        
        [Header(Color)]
        _Color("Color", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Tags {"RenderType"="Opaque"}
        Pass
        {
        	Tags {
				"LightMode" = "ForwardBase"
			}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityLightingCommon.cginc"
            #include "UnityStandardBRDF.cginc"
            
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
                float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 normal : NORMAL;
                float3 viewDir : TEXCOORD1;
                float4 color : COLOR;
                float4 objectPos : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
               
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.viewDir = UnityWorldSpaceViewDir( mul(unity_ObjectToWorld, v.vertex));
                o.color = v.color;
                o.objectPos = v.vertex;
                return o;
            }

            sampler2D _ShadowTexture;
            sampler2D _HalfShadowTexture;
            float _ShadowToHalfThreshold;
            float _HalfToLightThreshold;
            
            float _RimAmount;
            fixed4 _RimColor;
            float _RimThreshold;
            fixed4 _Color;
            
            float _Emissive;
            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv) * i.color * _Color;
                               
                // Single directional light
                float ndotl = DotClamped(i.normal, _WorldSpaceLightPos0);

                // Full shadow 
                float shadowAmount = step(ndotl, _ShadowToHalfThreshold);
                fixed4 shadow = tex2D(_ShadowTexture, i.uv);
                
                // Half shadow amout
                float halfShadowAmount = step(ndotl, _HalfToLightThreshold) * 1 - shadowAmount;
                fixed4 halfShadow = tex2D(_HalfShadowTexture, i.uv);
                
                // Rim lighting 
                float ndotv = dot( normalize(i.viewDir), i.normal);
                fixed4 rim = smoothstep( _RimAmount - 0.01, _RimAmount + 0.01,  (1 - ndotv) * pow(ndotl, _RimThreshold)) * _RimColor;
                
                // Final rgb color
                col.rgb = (col.rgb - shadow.a * shadowAmount - halfShadow.a * halfShadowAmount) * _LightColor0.rgb;
                return fixed4(rim + col.rgb , col.a);
            }
            ENDCG
        }
    }
}
